﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
namespace Checkers.Logic

{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                SI_user(5);
            }
            catch (Exception e)
            { Console.WriteLine(e.Message); }
        }

        private static void SI_user(int deep)
        {
            Process Client = CreateProcess("Test_Client.py");
            Process Server = CreateProcess("Test_Server.py");

            const int r = 8;// number_of_fields_in_row = 8;
            const int pcs = 12;// number_of_pieces_per_player = 12;
            Draughts_checkers game = new Draughts_checkers(r, pcs);

            Checkers_AI AI = new Checkers_AI(game, Color.White, false);
            var black_key = game.Generate_player_key(Color.Black);

            List<TimeSpan> times = new List<TimeSpan>();
            var maxtime = new TimeSpan();
            while (game.Number_of_pieces(Color.Black) > 0 && game.Number_of_pieces(Color.White) > 0)
            {
                try
                {
                    if (AI.Color == game.Check_active_player())
                    {
                        var begin = DateTime.Now;
                        var the_best_move = AI.The_best_move(deep);
                        var end = DateTime.Now;
                        var time = end - begin;
                        if (maxtime < time)
                        { maxtime = time; }
                        times.Add(time);
                        Make_move_and_display_boards(ref game, AI.Key, the_best_move.Origin, the_best_move.Destination);
                        Server.StandardInput.WriteLine(the_best_move.Origin.X);
                        Server.StandardInput.WriteLine(the_best_move.Origin.Y);
                        Server.StandardInput.WriteLine(the_best_move.Destination.X);
                        Server.StandardInput.WriteLine(the_best_move.Destination.Y);
                    }
                    else
                    {
                        Console.WriteLine("\nOrigin");
                        Console.Write("X ");
                        var x = Int32.Parse(Console.ReadLine());
                        Console.Write("Y ");
                        var y = Int32.Parse(Console.ReadLine());
                        var Origin = new Coordinates(x, y);
                        Console.WriteLine("\nDestination");
                        Console.Write("X ");
                        x = Int32.Parse(Console.ReadLine());
                        Console.Write("Y ");
                        y = Int32.Parse(Console.ReadLine());
                        var Destination = new Coordinates(x, y);
                        Make_move_and_display_boards(ref game, black_key, Origin, Destination);
                        Client.StandardInput.WriteLine(Origin.X);
                        Client.StandardInput.WriteLine(Origin.Y);
                        Client.StandardInput.WriteLine(Destination.X);
                        Client.StandardInput.WriteLine(Destination.Y);
                    }
                }
                catch (Exception e)
                { Console.WriteLine(e.Message); }

            }
            Console.WriteLine("Max time: " + maxtime);
            Console.WriteLine("Avg time: " + times.Average(x => x.TotalSeconds));

            Client.Kill();
            Server.Kill();
        }

        private static void Display_board_helper(Draughts_checkers game, Color color)
        {
            Checkers_piece[,] board = game.Get_copy_of_board(color);
            int number_of_fields_in_row = game.Number_of_fields_in_row;
            Display_board_helper(board, number_of_fields_in_row, color);
        }
        private static void Display_board_helper(Checkers_piece[,] board, int _number_of_fields_in_row, Color color)
        {
            Console.Write("\n---");
            for (int i = 0; i < _number_of_fields_in_row; i++)
            { Console.Write(i + " "); }

            for (int i = 0; i < _number_of_fields_in_row; i++)//i is row
            {
                Console.Write("\n" + i + ". ");
                for (int j = 0; j < _number_of_fields_in_row; j++)//j is column
                {
                    if (board[i, j] == null)
                    { Console.Write("= "); }
                    else
                    { Console.Write(board[i, j].ToString() + " "); }
                }
            }
            Console.Write("\n---");
            for (int i = 0; i < _number_of_fields_in_row; i++)
            { Console.Write(i + " "); }
            Console.Write(color + "\n");
        }
        public static void Display_board(Draughts_checkers game)//displays a board of current player
        { Display_board_helper(game, game.Check_active_player()); }
        public static void Display_board(Draughts_checkers game, Color color)
        { Display_board_helper(game, color); }
        public static void Make_move_and_display_boards(ref Draughts_checkers game, int player_secret_key, Coordinates origin, Coordinates destination)
        {
            Console.Write("\n" + origin.ToString());
            Console.WriteLine(" -> " + destination.ToString());
            game.Make_move(player_secret_key, origin, destination);
            Display_board(game, game.Check_player_color(player_secret_key));
            Display_board(game, game.Check_active_player());
        }

        private static Process CreateProcess(string File) {
            Process p = new Process();
            p.StartInfo.FileName = "python";//Path.GetFullPath(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\..\Local\Programs\Python\Python36\python.exe");    //@"C:\Users\Marcin\AppData\Local\Programs\Python\Python36\python.exe";
            p.StartInfo.Arguments = @"C:\Users\Piotr\Desktop\Display\Tests\" + File;//Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\Display\Tests\" + File; //@"C:\Users\Marcin\Desktop\Display\Tests\" + File;

            //przekierowanie wejścia/wyjścia
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardInput = true;

            //bez wyświetlania okna
            p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            p.StartInfo.CreateNoWindow = true;
            p.Start();

            return p;
        }
    }
}