﻿using System;

namespace Checkers.Logic
{
    public class Rate
    {
        private Checkers_piece[,] _board_black;
        private Coordinates _origin;
        private Coordinates _destination;
        private int _number_of_fields_in_row;
        public Rate(int number_of_fields_in_row, Checkers_piece[,] board_black)
        {
            _number_of_fields_in_row = number_of_fields_in_row;
            _board_black = board_black;
        }
        public int Difference_in_kings(Color color)
        {
            int difference = Number_of_pieces(color, Type.King) - Number_of_pieces(Get_oponent(color), Type.King);
            return difference;
        }
        public int Difference_in_men(Color color)
        {
            int difference = Number_of_pieces(color, Type.Man) - Number_of_pieces(Get_oponent(color), Type.Man);
            return difference;
        }
        public int Final_rate(Color color)
        { return (Difference_in_kings(color) + Difference_in_men(color)); }
        private Color Get_oponent(Color color)
        {
            if (color == Color.White)
                return Color.Black;
            return Color.White;
        }

        public bool Equals(Rate a)
        {
            var b = (Compare_arrays(a.Board_black));
            var origins = (a.Origin == Origin);
            var destinations = (a.Destination == Destination);
            var n = (a.Number_of_fields_in_row == Number_of_fields_in_row);
            bool result = (a != null && b && origins && destinations && n);
            return result;
        }
        private bool Compare_arrays(Checkers_piece[,] board)
        {
            bool result = true;
            for (int i = 0; i < _number_of_fields_in_row && result == true; i++)
            {
                for (int j = 0; j < _number_of_fields_in_row && result == true; j++)
                {
                    {
                        if (board[i, j] != _board_black[i, j])
                        { result = false; }
                    }
                }
            }
            return result;
        }

        public Checkers_piece[,] Get_copy_of_board(Color color)
        {
            if (color == Color.Black)
            {
                var copy_of_board_black = _board_black.Clone() as Checkers_piece[,];
                return copy_of_board_black;
            }
            else
            { return Rotate_board(_board_black); }
        }
        private Checkers_piece[,] Rotate_board(Checkers_piece[,] board_to_rotate)
        {
            Checkers_piece[,] board_rotated = new Checkers_piece[_number_of_fields_in_row, _number_of_fields_in_row];//row,column

            for (int i = 0; i < _number_of_fields_in_row; i++)//i is row
            {
                for (int j = 0; j < _number_of_fields_in_row; j++)//j is column
                {
                    board_rotated[_number_of_fields_in_row - i - 1, _number_of_fields_in_row - j - 1] = board_to_rotate[i, j];
                }
            }
            return board_rotated;
        }

        public Checkers_piece[,] Board_black { get => _board_black.Clone() as Checkers_piece[,]; }
        public Coordinates Origin { get => _origin; set => _origin = value; }
        public Coordinates Destination { get => _destination; set => _destination = value; }
        public int Number_of_fields_in_row { get => _number_of_fields_in_row; }

        public int Number_of_pieces(Color color, Type type)
        {
            int number_of_pieces = 0;
            for (int i = 0; i < _number_of_fields_in_row; i++)
            {
                for (int j = 0; j < _number_of_fields_in_row; j++)
                {
                    try
                    {
                        if (_board_black[i, j].Color == color && _board_black[i, j].Type == type)
                        { number_of_pieces++; }
                    }
                    catch (Exception)
                    { }
                }
            }
            return number_of_pieces;
        }
        public int Number_of_pieces(Color color)
        {
            int number_of_pieces = 0;
            for (int i = 0; i < _number_of_fields_in_row; i++)
            {
                for (int j = 0; j < _number_of_fields_in_row; j++)
                {
                    try
                    {
                        if (_board_black[i, j].Color == color)
                        { number_of_pieces++; }
                    }
                    catch (Exception)
                    { }
                }
            }
            return number_of_pieces;
        }
    }
}
