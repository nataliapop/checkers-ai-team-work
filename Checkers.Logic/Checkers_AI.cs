﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Checkers.Logic
{
    public class Checkers_AI
    {
        private Draughts_checkers _game;
        private Color _color;
        private int _key;
        private int max_num_of_pieces;
        private bool stratedy;

        public int Key { get => _key; }
        public Color Color { get => _color; }

        public Checkers_AI(Draughts_checkers game, Color color, bool strategy_save)
        {
            _game = game;
            _key = game.Generate_player_key(color);
            _color = color;
            stratedy = strategy_save;
        }
        private void Display_board_helper(Checkers_piece[,] board, Color color, int number_of_fields_in_row)
        {
            Console.WriteLine("Display_board_helper");
            Console.Write("\n---");
            for (int i = 0; i < number_of_fields_in_row; i++)
            { Console.Write(i + " "); }

            for (int i = 0; i < number_of_fields_in_row; i++)//i is row
            {
                Console.Write("\n" + i + ". ");
                for (int j = 0; j < number_of_fields_in_row; j++)//j is column
                {
                    if (board[i, j] == null)
                    { Console.Write("= "); }
                    else
                    { Console.Write(board[i, j].ToString() + " "); }
                }
            }
            Console.Write("\n---");
            for (int i = 0; i < number_of_fields_in_row; i++)
            { Console.Write(i + " "); }
            Console.Write(color + "\n");
            Console.WriteLine("Display_board_helper END!");
        }
        private Draughts_checkers Simulate_a_move(Color active_player_color, Checkers_piece[,] original_board, Coordinates origin, Coordinates destination, Coordinates last_moved_piece_coords, Color last_moved_piece_coords_color)//does nor change original_board
        {
            Draughts_checkers simulate_game = new Draughts_checkers(_game.Number_of_fields_in_row, _game.Number_of_pieces_per_player);

            var white_key = simulate_game.Generate_player_key(Color.White);

            var black_key = simulate_game.Generate_player_key(Color.Black);

            simulate_game.Set_board(active_player_color, original_board);
            simulate_game.Last_moved_piece_coords = last_moved_piece_coords;
            simulate_game.Last_moved_piece_coords_color = last_moved_piece_coords_color;

            int active_simulation_player_key = 0;
            if (active_player_color == Color.Black)
            { active_simulation_player_key = black_key; }
            else
            { active_simulation_player_key = white_key; }
            simulate_game.Make_move(active_simulation_player_key, origin, destination);
            return simulate_game;
        }
        public Rate The_best_move(int deep)
        {
            if (stratedy)
            { max_num_of_pieces = -1; }
            else
            { max_num_of_pieces = -(_game.Number_of_pieces_per_player + 1); }
            return The_best_move(_game, _color, deep).First();
        }
        private List<Rate> The_best_move(Draughts_checkers original_game, Color active_player_color, int deep)
        {
            Rate current_rate = new Rate(original_game.Number_of_fields_in_row, original_game.Get_copy_of_board(Color.Black));
            var Number_of_fields_in_row = original_game.Number_of_fields_in_row;
            if (deep == 0 || current_rate.Number_of_pieces(active_player_color) == 0)
            {
                List<Rate> f_list = new List<Rate>();
                f_list.Add(current_rate);
                if (stratedy)
                {//chcemy zachowac jak najwiecej naszych pionkow
                    if (max_num_of_pieces < current_rate.Number_of_pieces(_color))
                    { max_num_of_pieces = current_rate.Number_of_pieces(_color); }
                }
                else
                {//chcemy zachowac jak najwieksza roznice pionkow, korzystna dla nas
                    if (max_num_of_pieces < current_rate.Final_rate(_color))
                    { max_num_of_pieces = current_rate.Final_rate(_color); }
                }
                return f_list;
            }
            List<Rate> list_of_rates = new List<Rate>();

            if (true)
            {//version 2
                int length_of_capturings = 0;
                var all_the_longest_possible_ways = original_game.Get_the_longest_capturings(original_game.Get_copy_of_board(active_player_color), ref length_of_capturings);

                if (length_of_capturings > 0)//jesli sa mozliwe bicia to one sa obowiazkowe
                {
                    foreach (var way in all_the_longest_possible_ways)
                    {
                        Draughts_checkers game = new Draughts_checkers(Number_of_fields_in_row, Number_of_fields_in_row);
                        game.Set_board(active_player_color, original_game.Get_copy_of_board(active_player_color));
                        game.Last_moved_piece_coords = original_game.Last_moved_piece_coords;
                        game.Last_moved_piece_coords_color = original_game.Last_moved_piece_coords_color;
                        try
                        {
                            for (int step_ID = 0; step_ID + 1 < way.Count(); step_ID++)//symulujemy wykonanie wszystkich bic danego gracza od razu
                            {
                                game = Simulate_a_move(active_player_color, game.Get_copy_of_board(active_player_color), way[step_ID], way[step_ID + 1], game.Last_moved_piece_coords, game.Last_moved_piece_coords_color);
                            }
                            List<Rate> local_list_of_rates = new List<Rate>();
                            try
                            {
                                local_list_of_rates = The_best_move(game, game.Check_active_player(), deep - 1);
                            }
                            catch (Exception e)
                            { }

                            foreach (var rate in local_list_of_rates)
                            {
                                rate.Origin = way[0];
                                rate.Destination = way[1];
                            }
                            list_of_rates = list_of_rates.Concat(local_list_of_rates).ToList();
                        }
                        catch (Exception e)
                        { }
                    }
                }
                else
                {
                    var original_game_board = original_game.Get_copy_of_board(active_player_color);
                    List<Coordinates> possible_origins = new List<Coordinates>();

                    //okreslenie pol z naszymi pionkami
                    for (int i = 0; i < original_game.Number_of_fields_in_row; i++)//row
                    {
                        for (int j = 0; j < original_game.Number_of_fields_in_row; j++)//column
                        {
                            if (((i + j) % 2 != 0))//czy pole jest grywalne
                            {
                                if (original_game_board[i, j] != null && original_game_board[i, j].Color == active_player_color)
                                { possible_origins.Add(new Coordinates(j, i)); }
                            }
                        }
                    }

                    foreach (var origin in possible_origins)
                    {
                        //to sa pola ktoe musza byc puste zeby dalo sie wykonac jakikolwiek ruch
                        //pionek moze sie przesunac na ktores z tych pol
                        //jesli ktorez z tych pol z pary pair1 i par2 sa wolne to szuka po przekatnej wszystkich ruchow
                        List<Coordinates> pair1 = new List<Coordinates>();
                        pair1.Add(new Coordinates(origin.X - 1, origin.Y - 1));
                        pair1.Add(new Coordinates(origin.X + 1, origin.Y - 1));
                        pair1.Add(new Coordinates(origin.X + 1, origin.Y + 1));
                        pair1.Add(new Coordinates(origin.X - 1, origin.Y + 1));

                        List<Coordinates> pair2 = new List<Coordinates>();
                        pair2.Add(new Coordinates(origin.X - 2, origin.Y - 2));
                        pair2.Add(new Coordinates(origin.X + 2, origin.Y - 2));
                        pair2.Add(new Coordinates(origin.X + 2, origin.Y + 2));
                        pair2.Add(new Coordinates(origin.X - 2, origin.Y + 2));

                        if (original_game_board[origin.Y, origin.X].Type == Type.Man)
                        {
                            List<Coordinates> fields_for_men = new List<Coordinates>();
                            fields_for_men = pair1.Concat(pair2).ToList();
                            foreach (var empty_field in fields_for_men)
                            {
                                try
                                {
                                    if (original_game_board[empty_field.Y, empty_field.X] == null)
                                    {
                                        var begin_rate = new Rate(Number_of_fields_in_row, original_game.Get_copy_of_board(_color));
                                        var game = Simulate_a_move(active_player_color, original_game.Get_copy_of_board(active_player_color), origin, empty_field, original_game.Last_moved_piece_coords, original_game.Last_moved_piece_coords_color);
                                        var end_rate = new Rate(Number_of_fields_in_row, game.Get_copy_of_board(_color));

                                        if (max_num_of_pieces >= end_rate.Number_of_pieces(_color) && stratedy == true)
                                        { }
                                        else if (max_num_of_pieces >= end_rate.Final_rate(_color) && stratedy == false)
                                        { // jesli stracilismy wiecej niz jeden pionek przy tym ruchu to zrob ciecie w przeszukiwaniu drzewa
                                          //jesli na tym etapie mamy mniej pionkow niz daje nam najlepsza sciezka to utnij drzewo
                                        }
                                        else
                                        {
                                            var local_list_of_rates = The_best_move(game, game.Check_active_player(), deep - 1);
                                            foreach (var rate in local_list_of_rates)
                                            {
                                                rate.Origin = origin;
                                                rate.Destination = empty_field;
                                            }
                                            list_of_rates = list_of_rates.Concat(local_list_of_rates).ToList();
                                        }
                                    }
                                }
                                catch (Exception e)
                                { }
                            }
                        }
                        else if (original_game_board[origin.Y, origin.X].Type == Type.King)
                        {
                            for (int pair_ID = 0; pair_ID < pair1.Count(); pair_ID++)
                            {
                                bool shall_we_search = false;
                                try
                                {
                                    if (original_game_board[pair1[pair_ID].Y, pair1[pair_ID].X] == null)
                                    {
                                        shall_we_search = true;
                                    }
                                    else if (original_game_board[pair2[pair_ID].Y, pair2[pair_ID].X] == null)
                                    { shall_we_search = true; }
                                    else { shall_we_search = false; }
                                }
                                catch (Exception e)
                                { shall_we_search = false; }//wyjatek oznacza, ze te pola nie istnieja wiec nie warto szukac

                                if (shall_we_search == true)
                                {
                                    for (int j = 1; j < Number_of_fields_in_row; j++)//j to odleglosc pomiedzy nasza dama a polem na ktore chcemy ja przesunac
                                    {
                                        List<Coordinates> dests = new List<Coordinates>();
                                        dests.Add(new Coordinates(origin.X - j, origin.Y - j));
                                        dests.Add(new Coordinates(origin.X + j, origin.Y - j));
                                        dests.Add(new Coordinates(origin.X + j, origin.Y + j));
                                        dests.Add(new Coordinates(origin.X - j, origin.Y + j));

                                        for (int dest_ID = 0; dest_ID < dests.Count(); dest_ID++)
                                        {
                                            try
                                            {
                                                var begin_rate = new Rate(Number_of_fields_in_row, original_game.Get_copy_of_board(_color));
                                                var game = Simulate_a_move(active_player_color, original_game.Get_copy_of_board(active_player_color), origin, dests[dest_ID], original_game.Last_moved_piece_coords, original_game.Last_moved_piece_coords_color);

                                                var end_rate = new Rate(Number_of_fields_in_row, game.Get_copy_of_board(_color));

                                                if (max_num_of_pieces >= end_rate.Number_of_pieces(_color) && stratedy == true)
                                                { }
                                                else if (max_num_of_pieces >= end_rate.Final_rate(_color) && stratedy == false)
                                                { // jesli stracilismy wiecej niz jeden pionek przy tym ruchu to zrob ciecie w przeszukiwaniu drzewa
                                                  //jesli na tym etapie mamy mniej pionkow niz daje nam najlepsza sciezka to utnij drzewo
                                                }
                                                else
                                                {
                                                    var local_list_of_rates = The_best_move(game, game.Check_active_player(), deep - 1);
                                                    foreach (var rate in local_list_of_rates)
                                                    {
                                                        rate.Origin = origin;
                                                        rate.Destination = dests[dest_ID];
                                                    }
                                                    list_of_rates = list_of_rates.Concat(local_list_of_rates).ToList();
                                                }
                                            }
                                            catch (Exception)
                                            { }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            int final_rate = -(_game.Number_of_fields_in_row * _game.Number_of_fields_in_row);
            List<Rate> final_list_of_rates = new List<Rate>();

            foreach (var rate in list_of_rates)
            {
                if (rate.Final_rate(active_player_color) > final_rate)
                {
                    final_rate = rate.Final_rate(active_player_color);
                    final_list_of_rates = new List<Rate>();
                    final_list_of_rates.Add(rate);
                }
                else if (rate.Final_rate(active_player_color) == final_rate && (final_list_of_rates.Exists(x => x.Equals(rate)) == false))
                {
                    final_list_of_rates.Add(rate);
                }
            }
            return final_list_of_rates;
        }
    }
}
